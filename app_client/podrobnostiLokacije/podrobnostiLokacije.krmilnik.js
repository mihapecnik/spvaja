(function() {
  /* global angular */
  
 function podrobnostiLokacijeCtrl($window, $routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;
    vm.idLokacije = $routeParams.idLokacije;
    
   vm.jePrijavljen = avtentikacija.jePrijavljen();
    
   vm.prvotnaStran = $location.path();
    
    var razdalja = function() {
      var razdalje=JSON.parse($window.localStorage['razdaljeLokacij']);
      for (var i in razdalje) {
        if (vm.idLokacije == razdalje[i]._id){
          return razdalje[i].razdalja;
        }
      }
      return '?';
    }
    
    vm.razdalja = razdalja();
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
    
    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              razdalja: vm.razdalja,
              nazivLokacije: vm.podatki.lokacija.naziv
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
          if (typeof podatki != 'undefined')
            vm.podatki.lokacija.komentarji.push(podatki);
      });
    };
  }
 podrobnostiLokacijeCtrl.$inject = ['$window','$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();